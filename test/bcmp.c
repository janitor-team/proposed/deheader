/*
 * Items: bcmp(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <strings.h>

main(int arg, char **argv)
{
    (void) bcmp("aaa", "bbb", 3);
}
