/*
 * Items: cfsetispeed(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <termios.h>

main(int arg, char **argv)
{
    struct termios t;
    cfsetispeed(&t, 0);
}
